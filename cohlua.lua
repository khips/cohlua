-- The standard library for Cohlua.

local Handlers = {}
local Commands = {
	load = function(arg, out)
		chunk = loadfile(arg)
		local succ, val = pcall(chunk)

		if not succ then
			out({error=val})
		end
	end,

	x = function(arg, out)
		out(_G[arg])
	end,

	re = function(arg, out)
		-- body
	end,

	st = function(arg, out)

	end
}

local rich_output = cohlua.json_output;

function cohlua.dump_table(t)
	local val = "{\n"
	for k, v in pairs(t) do
		val = val .. "\t" .. k .. " = " .. tostring(v) .. "\n"
	end

	return val .. "}"
end

local EvaluationEnv = {}
setmetatable(EvaluationEnv, {__index=_G})

function cohlua._received_data(socket, data)
	-- Trim the input:
	data = data:match("^%s*(.-)%s*$")

	local function outfn(output)
		cohlua.rich_output(socket, output)
	end

	local handler = Handlers["data"]
	if handler then
		local succ, stop = pcall(handler, data, outfn)

		if succ then
			return stop
		else
			info = debug.getinfo(handler)
			fn_name = info.name
			if not fn_name then
				fn_name = "line " .. info.linedefined
			end

			cohlua.output(socket, "Error while executing handler function: " .. stop)
		end
	else
		if data:sub(1, 1) == "%" then
			-- Strings beginning with % are interpreted as commands.
			local space, _ = data:find(" ", 1, true)
			local command
			local arg = nil

			if space then
				command = data:sub(2, space-1):lower()
				command = command:lower()
				arg = data:sub(space + 1, -1)
			else
				command = data:sub(2)
			end

			local handler = Commands[command]

			if handler then
				local succ, stop = pcall(handler, arg, outfn, {input=data, command=command})

				if succ then
					return stop
				else
					cohlua.output(socket, "Error while executing handler for command: " .. command)
				end
			end
		else
			function EvaluationEnv.print(txt)
				cohlua.output(socket, tostring(txt) .. "\n")
			end
			chunk = load(data, nil, "t", EvaluationEnv)
			if chunk then
				chunk()
			end
		end
	end
end

function cohlua.command_handler(command, fn)
	Commands[command] = fn
end

function cohlua.set_command_handlers(tab)
	-- Replace the current table of debug command handlers with the
	-- specified table tab.
	Commands = tab
end

function cohlua.clear_commands()
	Commands = {}
end

function cohlua.on_data(fn)
	-- Register a new function to handle data received
	-- from the socket.
	Handlers["data"] = fn
end

local TouchEventsReceived = false
local startReceivingTouchEvents = cohlua._register_for_touch
-- Prevent
cohlua._register_for_touch = nil

function cohlua.on_touch(fn)
	if not TouchEventsReceived then
		startReceivingTouchEvents()
	end

	Handlers["touch"] = fn
	
	if fn then

	end
end
