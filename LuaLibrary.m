//
//  LuaLibrary.m
//  Cohlua
//
//  Created by Brian on 12/14/12
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import "LuaLibrary.h"
#import "LuaState.h"
#import "CohluaLua.h"

// Retrieve the value stored in the global.  If it does not exist,
// initialize it to a table.  Returns the stored value or new table
// as the top stack value.
static void cohlua_global_table(lua_State *L, const char *name) {
	lua_getglobal(L, name);
	if (lua_isnil(L, -1)) {
		lua_newtable(L);
		lua_pushvalue(L, -1);
		lua_setglobal(L, name);
	}
}

@implementation LuaLibrary

//- (id)initWithName:(NSString *)name luaState:(LuaState *)lua {
//	if ((self = [super init])) {
//		luaState = lua;
//		[name getCString:libName maxLength:128 encoding:NSASCIIStringEncoding];
//	}
//
//	return self;
//}
//
//// Adding and removing functions.
//- (void)addLibraryFunction:(lua_CFunction)cfunc withName:(NSString *)fname {
//    lua_State *L = luaState.state;
//    
//    lua_getglobal(L, libName);
//    if (lua_isnil(L, -1)) {
//        @throw [NSException exceptionWithName:@"LibraryNotLoaded"
//                                       reason:@"Can't add function when library is not loaded"
//                                     userInfo:nil];
//    }
//    lua_pushcfunction(L, cfunc);
//    lua_setfield(L, -2, [fname cStringUsingEncoding:LUA_STRING_ENCODING]);
//    lua_pop(L, 1);
//}
//
//- (void)addLibraryBlock:(int (^)())block withName:(NSString *)fname {
//    lua_getglobal(L, "cohlua");
//    cohlua_push_block(L, block);
//	lua_setfield(L, -2, [fname cStringUsingEncoding:LUA_STRING_ENCODING]);
//}
//
//- (void)loadFunctions:(luaL_Reg *)lib_f {
//	lua_State *L = luaState.state;
//
//	cohlua_global_table(L, libName);
//
//	luaL_setfuncs(L, lib_f);
//	lua_pop(L, 1);
//}
//
//- (void)setName:(NSString *)name {
//	[name getCString:libName maxLength:128 encoding:NSASCIIStringEncoding];
//}

@end
