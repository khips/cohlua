//
//  CohluaUITest.m
//  Cohlua
//
//  Created by Brian Sanders on 12/9/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import "CohluaUITest.h"

@implementation CohluaUITest

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"Started");
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
