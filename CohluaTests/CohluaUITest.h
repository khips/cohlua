//
//  CohluaUITest.h
//  Cohlua
//
//  Created by Brian Sanders on 12/9/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CohluaUITest : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
