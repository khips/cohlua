//
//  CohluaTests.m
//  CohluaTests
//
//  Created by labuser on 11/11/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CohluaTests.h"

@implementation CohluaTests

- (void)setUp
{
    [super setUp];
    lua = [[LuaState alloc] init];
}

- (void)tearDown
{
    // Tear-down code here.
    lua = nil;
    [super tearDown];
}

- (void)testVariables
{
    NSString *inVal = @"Hello, world!";
    [lua setVariable:@"myval" toValue:inVal];
    NSString *outVal = [lua valueForVariable:@"myval"];
    
    STAssertTrue([inVal isEqualToString:outVal], @"Simple variable set/get failed.");
    
    [lua setVariable:@"newtab.w.myval" toValue:inVal creatingTables:YES];
    outVal = [lua valueForVariable:@"newtab.w.myval"];
    STAssertTrue([inVal isEqualToString:outVal], @"Nested variable set/get failed.");
    
    @try {
        [lua setVariable:@"anothertab.w.myval" toValue:inVal];
    }
    @catch (VariableIsNotATable *exception) {
        STAssertTrue(YES, @"Should have encountered error.");
    }
}

- (void)testUserdataGC {
    lua_State *L = lua.state;
    NSDictionary *myThing = @{@"Hello" : @"World"};
    
    CFIndex refcountBefore = CFGetRetainCount((__bridge CFTypeRef)myThing);
    NSLog(@"Refcount before push: %li", refcountBefore);
    
    cohlua_push_full_object(L, myThing);
    
    CFIndex refcountPush = CFGetRetainCount((__bridge CFTypeRef)myThing);
    NSLog(@"Refcount after push: %li", refcountPush);
    STAssertEquals(refcountBefore+1, refcountPush, @"Refcount of user data object was not incremented.");
    
    lua_pop(L, 1);
    lua_gc(L, LUA_GCCOLLECT, 0);
    
    CFIndex refcountAfter = CFGetRetainCount((__bridge CFTypeRef)myThing);
    NSLog(@"Refcount after pop: %li", refcountAfter);
    
    // The count before push and after pop should be equal:
    STAssertEquals(refcountBefore, refcountAfter, @"Refcount of user data object was not decremented after pop.");
}

- (void)testREPL {
    dispatch_queue_t queue = dispatch_queue_create("com.statori.cohlua.test",
                                                   NULL);
    [lua startRemoteInterfaceOnPort:3240 withQueue:queue];
    sleep(1800);
}

- (void)testBlockCalling {
	const char* strval = "Hello!";
	cohlua_push_block(lua.state, ^(lua_State *L) {
		lua_pushstring(L, strval);

		return 1;
	});
    
    lua_State *L = lua.state;

	lua_call(L, 0, 1);
	lua_pushstring(L, strval);

	STAssertTrue(lua_compare(L, -1, -2, LUA_OPEQ),
				 @"Block failed to return expected value.");
}
@end









