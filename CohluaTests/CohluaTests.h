//
//  CohluaTests.h
//  CohluaTests
//
//  Created by brian on 11/11/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "LuaState.h"
#import "GCDAsyncSocket+SimpleProtocol.h"

@interface CohluaTests : SenTestCase {
    LuaState *lua;
}

- (void)testVariables;
- (void)testUserdataGC;

// Keeps the remote interface open for 30 minutes.
- (void)testREPL;
- (void)testBlockCalling;

@end
