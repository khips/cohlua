//
//  CohluaLua.m
//  Cohlua
//
//  Created by Brian Sanders on 11/17/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import "CohluaLua.h"
#import "CohluaObjectInterface.h"

#import "SBJson.h"

#import <sys/utsname.h>
#import <UIKit/UIScreen.h>
#import <UIKit/UIDevice.h>

@implementation VariableIsNotATable
@end

@implementation VariableIsNotAFunction
@end

@implementation InsufficientStackSpace
@end

// helper functions:
static NSDictionary *lua_to_NSDictionary(lua_State *L, int index) {
    // Pull the table at the specified index into an NSDictionary.
    NSMutableDictionary *dict = [NSMutableDictionary new];
    id key;
    
    lua_pushnil(L); // first key
    // Adjust index, since we've pushed another value on the stack.
    if (index < 0) index--;
    
    while (lua_next(L, index) != 0) {
        // Only add the keys if they are strings or numbers.
        if (lua_isstring(L, -2)) {
            
            key = [NSString stringWithCString:lua_tostring(L, -2)
                                     encoding:LUA_STRING_ENCODING];
        } else if (lua_isnumber(L, -2))
            key = [NSNumber numberWithInt:(int)lua_tonumber(L, -2)];
        else
            key = nil;
        
        if (key)
            dict[key] = lua_to_objc(L, -1);
        
        lua_pop(L, 1);  // pop the value; leave the key
    }
    
    return dict;
}

NSString *cohlua_to_NSString(lua_State *L, int index) {
    if (lua_isstring(L, index))
        return [NSString stringWithCString:lua_tostring(L, index)
                                  encoding:LUA_STRING_ENCODING];
    else
        return @"";
}

NSArray *cohlua_to_NSArray(lua_State *L, int index) {
    NSMutableArray *arr = [NSMutableArray new];
    id val;
    
    lua_pushnil(L);
    if (index < 0) index--;
    
    while (lua_next(L, index) != 0) {
        val = lua_to_objc(L, -1);
        [arr addObject:val];
        lua_pop(L, 1); // pop the value
    }
    
    return arr;
}

NSArray *cohlua_stack_to_NSArray(lua_State *L) {
    int top = lua_gettop(L);
    NSMutableArray *stack = [[NSMutableArray alloc] initWithCapacity:top];
    
    for (int i = 1; i <= top; i++) {
        [stack addObject:lua_to_objc(L, i)];
    }
    
    return stack;
}

BOOL cohlua_is_callable(lua_State *L, int index) {
    if (lua_isfunction(L, index))
        return YES;
    
    if (lua_istable(L, index)) {
        lua_getmetatable(L, index);
        lua_getfield(L, -1, "__call");
    }
    
    return NO;
}

int cohlua_full_pcall(lua_State *L, int nargs, int nresults, int errfunc) {
    int fpos = -1 - nargs;
    
    if (lua_isfunction(L, fpos)) {
        return lua_pcall(L, nargs, nresults, errfunc);
    }
    
    if (lua_getmetatable(L, fpos)) {
        lua_getfield(L, -1, "__call");
        // Remove metatable:
        lua_remove(L, -2);
        if (lua_isfunction(L, -1)) {
            // Insert the function
            if (errfunc < 0)
                errfunc -= 1;
            
            lua_insert(L, lua_gettop(L) + fpos - 1);
            lua_pop(L, 1);
            
            return lua_pcall(L, nargs+1, nresults, errfunc);
        } else {
            // Pop the value of __call, the arguments, and the
            // table (not) being called.
			// Note that this is consistent with the behavior of Lua: 
			// __call must be a function, not a callable object.
            lua_pop(L, 1 + nargs + 1);
            lua_pushstring(L, "Attempt to call non-function.");
            lua_error(L);
        }
    }
    
    lua_pop(L, nargs + 1);
    lua_pushstring(L, "Attempt to call non-function.");
    lua_error(L);
    
    return 0;
}

NSString* cohlua_call_tostring(lua_State *L, int index) {
    // Convert the lua object at index index to a string by calling the
    // tostring() function on it.
    
    index = (index < 0) ? index - 1 : index;
    lua_getglobal(L, "tostring");
    lua_pushvalue(L, index);
    int err = lua_pcall(L, 1, 1, 0);
    
    if (err) {
        lua_pop(L, 1);
		return @"";
    } else {
        // Successfully called tostring();
		NSString *output = [NSString stringWithCString:lua_tostring(L, -1)
									    encoding:LUA_STRING_ENCODING];
        lua_pop(L, 1);
		return output;
    }
}

id lua_to_objc(lua_State *L, int index) {
    switch (lua_type(L, -1)) {
        case LUA_TNUMBER:
            return [NSNumber numberWithDouble:lua_tonumber(L, index)];
        case LUA_TSTRING:
            return cohlua_to_NSString(L, index);
        case LUA_TTABLE:
            if (cohlua_is_numeric_table(L, index))
                return cohlua_to_NSArray(L, index);
            return lua_to_NSDictionary(L, index);
        case LUA_TBOOLEAN:
            return [NSNumber numberWithBool:lua_toboolean(L, index)];
		case LUA_TUSERDATA:
			// How to safely handle this?
			// Need to check if the userdata value is a valid
			// Objective-C object.
			return cohlua_to_object(L, index);
			// return cohlua_safe_to_object(L, index);
        default:
            return cohlua_call_tostring(L, index);
    }
    
    return nil;
}

// Runs a heuristic to determine if the table at the given
// index is a numeric table (i.e., array) or a dictionary
// style table.  Assumes that it is a table!  Does not
// check!
BOOL cohlua_is_numeric_table(lua_State *L, int index) {
    int tcount = lua_rawlen(L, index);
    
    if (tcount > 0) {
        lua_rawgeti(L, index, 1);
        if (lua_isnil(L, -1))
            return NO;
        lua_pop(L, 1);
        
        lua_rawgeti(L, index, tcount);
        if (lua_isnil(L, -1))
            return NO;
        lua_pop(L, 1);
        return YES;
    } else {
        return NO;
    }
}


NSString* cohlua_to_printable(lua_State *L, int index) {
    // Convert the value at the specified index to a string.
    // Does not alter the stack.
    
    id val = lua_to_objc(L, index);
    return [val description];
}

static void lua_push_NSDictionary(lua_State *L, NSDictionary *dict) {
    // Push a table onto the lua stack.
    lua_createtable(L, 0, [dict count]);
    
    for (id key in dict) {
        if ([key isKindOfClass:[NSString class]]) {
            cohlua_push_from_objc(L, dict[key]);
            lua_setfield(L, -2, [key cStringUsingEncoding:LUA_STRING_ENCODING]);
        }
    }
}

static void lua_push_NSArray(lua_State *L, NSArray *array) {
    lua_createtable(L, [array count], 0);
    
    int i = 1;  // Lua arrays begin at one
    for (id val in array) {
        cohlua_push_from_objc(L, val);
        lua_rawseti(L, -2, i++);
    }
}

void cohlua_push_NSString(lua_State *L, NSString *str) {
    lua_pushstring(L, [str cStringUsingEncoding:LUA_STRING_ENCODING]);
}

///// Pushing user data ('heavy')
static int cohlua_perform_gc(lua_State *L) {
    __strong id* obj = (__strong id *)lua_touserdata(L, 1);
    *obj = nil;
    
    return 0;
}

// Push an object onto the stack without converting it to
// its corresponding Lua type.  Add a metatable that handles
// garbage collection.
void cohlua_push_object(lua_State *L, id obj) {
    __weak id* userdata = (__weak id *)lua_newuserdata(L, sizeof(id));
    CFRetain((__bridge CFTypeRef)(obj));
    *userdata = obj;
    
    if (luaL_newmetatable(L, "_cohlua_userdata")) {
        // Set up the metatable.
        lua_pushcfunction(L, cohlua_perform_gc);
        lua_setfield(L, -2, "__gc");
    }
    
    lua_setmetatable(L, -2);
}

id cohlua_safe_to_object(lua_State *L, int index) {
	// Verify that the value at the specified index is in fact a
	// pointer to a valid Objective-C object before returning. Return
	// nil otherwise.

	id val;

	// Change signal handler in order to catch bad accesses.
}

BOOL cohlua_is_block(lua_State *L, int index) {
	// Is the value at the specified index on the stack a valid block?

	// TODO: implement.
}

id cohlua_to_object(lua_State *L, int index) {
	// Why is this a weak reference?
    __weak id* object = (__weak id*)lua_touserdata(L, index);
    return *object;
}

void cohlua_push_selector(lua_State *L, SEL sel_) {
    SEL *userdata = lua_newuserdata(L, sizeof(SEL));
    *userdata = sel_;
}

// If the datum at index is not a selector, everything
// will explode.
SEL cohlua_to_selector(lua_State *L, int index) {
    SEL *selPtr = lua_touserdata(L, index);
    return *selPtr;
}

static void cohlua_push_return_value(lua_State *L, NSInvocation *invoke) {
    NSMethodSignature *sig = [invoke methodSignature];
    const char *retType = [sig methodReturnType];
    if (!strcmp(retType, @encode(id))) {
        id returnedObj;
        [invoke getReturnValue:&returnedObj];
        cohlua_push_from_objc(L, returnedObj);
    } else if (!strcmp(retType, @encode(char *))) {
        char *retString;
        [invoke getReturnValue:&retString];
        lua_pushstring(L, retString);
    } else if (!strcmp(retType, @encode(int))) {
        int retInt;
        [invoke getReturnValue:&retInt];
        lua_pushinteger(L, retInt);
    } else if (!strcmp(retType, @encode(BOOL))) {
        BOOL retBool;
        [invoke getReturnValue:&retBool];
        lua_pushboolean(L, retBool);
    } else {
        lua_pushnil(L);
    }
}

// Make an object whose methods are fully accessible
// within Lua?
// Experimental.
static int cohlua_call_NSInvocation(lua_State *L) {
	int i = lua_gettop(L);
	NSInvocation *invok = cohlua_to_object(L, lua_upvalueindex(1));

	for (int j = 1; j <= i; j++) {
		id arg = lua_to_objc(L, j);
		[invok setArgument:&arg atIndex:1+j];
	}

	[invok invoke];
	cohlua_push_return_value(L, invok);

	return 1;
}

static int cohlua_index_object(lua_State *L) {
    if (lua_gettop(L) != 2) {
        lua_pushstring(L, "__index expects exactly two arguments.");
        lua_error(L);
    }
    id object = cohlua_to_object(L, 1);
    
    if (lua_isnumber(L, 2)) {
        int index = lua_tointeger(L, 2);
        lua_pop(L, 2);
        
        if ([object respondsToSelector:@selector(objectAtIndexedSubscript:)]) {
            @try {
                cohlua_push_from_objc(L, [object objectAtIndexedSubscript:index]);
            }
            @catch (NSException *exception) {
                lua_pushnil(L);
            }
            return 1;
        }
    }
    NSString *methodName = cohlua_to_NSString(L, 2);
    NSString *selName = [methodName stringByReplacingOccurrencesOfString:@"_"
                                                              withString:@":"];
    SEL sel = NSSelectorFromString(selName);
    
    if (![object respondsToSelector:sel]) {
        lua_pushnil(L);
        return 1;
    }
    
    NSMethodSignature *sig = [object methodSignatureForSelector:sel];
    NSInvocation *invoke = [NSInvocation invocationWithMethodSignature:sig];
    [invoke setTarget:object];
    [invoke setSelector:sel];
    
    // Property getter:
    if ([sig numberOfArguments] == 2) {
        [invoke invoke];
        cohlua_push_return_value(L, invoke);

		return 1;
    }

	// Not a property getter, so proceed to generate a function.
	cohlua_push_object(L, sig);

    
    return 0;
}


void cohlua_push_full_object(lua_State *L, id object) {
    __weak id* userdata = (__weak id *)lua_newuserdata(L, sizeof(id));
    CFRetain((__bridge CFTypeRef)(object));
    *userdata = object;
    
    if (luaL_newmetatable(L, "CohluaFull")) {
        lua_pushcfunction(L, cohlua_index_object);
        lua_setfield(L, -2, "__index");
        lua_pushcfunction(L, cohlua_perform_gc);
        lua_setfield(L, -2, "__gc");
    }
    lua_setmetatable(L, -2);
}

// CohluaObjectInterface stuff.
static int _cohlua_interface_caller(lua_State *L, SEL sel, int args, 
									id (^callBlock)(),
									int (^defaultBlock)(lua_State *L)) {
	id<CohluaObjectInterface> val = cohlua_to_object(L, 1);

	if (! [val respondsToSelector:sel]) {
		// What would Lua do?
		lua_pushnil(L);
		return 1;
	}

	NSString *lookup = cohlua_call_tostring(L, 2);
	
	@try {
		cohlua_push_from_objc(L, [val cohluaIndexMethod:lookup]);
		return 1;
	} 
	@catch (NSException *exc) {
		lua_pushstring(L, "Error in __index implementation.");
		lua_error(L);
	}
}

static int _cohlua_call_index(lua_State *L) {

}

static int _cohlua_call_newindex(lua_State *L) {
	id<CohluaObjectInterface> val = cohlua_to_object(L, 1);
	BOOL runDefault = NO;

	if (! [val respondsToSelector:@selector(cohluaNewIndexKey:value:)]) {
		runDefault = YES;
	} else {
		@try {
			NSString *key = cohlua_call_tostring(L, 2);
			id val = lua_to_objc(L, 3);
			id retVal = [val cohluaNewIndexKey:key value:val];

		}
		@catch (NSException *exc) {
		
		}
	}
}

void cohlua_push_object_with_interface(lua_State *L, id val) {
	// TODO: Implement me!
	cohlua_push_object(L, val);
	lua_getmetatable(L, -1);
}

/////////////////

void cohlua_push_from_objc(lua_State *L, id val) {
    // Push an Objective-C object onto the top of the stack.
    if ([val isKindOfClass:[NSNumber class]]) {
        lua_pushnumber(L, [(NSNumber *)val doubleValue]);
    } else if ([val isKindOfClass:[NSString class]]) {
        lua_pushstring(L, [(NSString *)val cStringUsingEncoding:LUA_STRING_ENCODING]);
    } else if ([val isKindOfClass:[NSDictionary class]]) {
        lua_push_NSDictionary(L, val);
    } else if ([val isKindOfClass:[NSArray class]]) {
        lua_push_NSArray(L, val);
    } else if (val == nil) {
        lua_pushnil(L);
    } else if ([val conformsToProtocol:@protocol(CohluaObjectInterface)]) {
		cohlua_push_object_with_interface(L, val);
    } else {
        cohlua_push_full_object(L, val);
    }
}

void cohlua_push_items_from_NSArray(lua_State *L, NSArray *array) {
    if (lua_checkstack(L, [array count])) {
        for (id val in array) {
            cohlua_push_from_objc(L, val);
        }
    } else {
        @throw [InsufficientStackSpace exceptionWithName:@"InsufficientStackSpace"
                                                  reason:@"Could not push array items onto Lua stack."
                                                userInfo:nil];
    }
}

void cohlua_push_from_objc_checked(lua_State *L, id val) {
    if (!lua_checkstack(L, 1)) {
        @throw [InsufficientStackSpace exceptionWithName:@"InsufficientStackSpace"
                                                  reason:@""
                                                userInfo:nil];
    }
    cohlua_push_from_objc(L, val);
}

NSData* cohlua_to_json_data(lua_State *L, int index) {
    if (lua_isfunction(L, index)) {
        lua_pushstring(L, "Object cannot be encoded.");
        lua_error(L);
    }
    
    id val = lua_to_objc(L, index);
    SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    return [writer dataWithObject:val];
}

// Cohlua standard library functions:    /////
int cohlua_platform_info(lua_State *L) {
    struct utsname platinfo;
    uname(&platinfo);
    
    lua_newtable(L);
    lua_pushstring(L, platinfo.sysname);
    lua_setfield(L, -2, "sysname");
    
    lua_pushstring(L, platinfo.machine);
    lua_setfield(L, -2, "machine");
    
    UIScreen *screen = [UIScreen mainScreen];
    CGRect frame = [screen bounds];
    lua_pushnumber(L, frame.size.width);
    lua_setfield(L, -2, "screen_width");
    
    lua_pushnumber(L, frame.size.height);
    lua_setfield(L, -2, "screen_height");
    
    lua_pushnumber(L, [screen scale]);
    lua_setfield(L, -2, "scale");
    
    UIDevice *device = [UIDevice currentDevice];
    cohlua_push_NSString(L, device.systemVersion);
    lua_setfield(L, -2, "system_version");
    
    UIDeviceOrientation orientation = device.orientation;
    NSString *orientationName;
    
    switch (orientation) {
        case UIDeviceOrientationFaceDown:
            orientationName = @"FaceDown";
            break;
        case UIDeviceOrientationFaceUp:
            orientationName = @"FaceUp";
            break;
        case UIDeviceOrientationLandscapeLeft:
            orientationName = @"LandscapeLeft";
            break;
        case UIDeviceOrientationLandscapeRight:
            orientationName = @"LandscapeRight";
            break;
        case UIDeviceOrientationPortrait:
            orientationName = @"Portrait";
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            orientationName = @"PortraitUpsideDown";
            break;
        case UIDeviceOrientationUnknown:
            orientationName = @"Unknown";
    }
    
    cohlua_push_NSString(L, orientationName);
    lua_setfield(L, -2, "orientation");
    
    return 1;
}

void cohlua_load_functions(lua_State *L, luaL_Reg *lib_f, char *libname) {
	lua_newtable(L);
	luaL_setfuncs(L, lib_f, 0);
	lua_setglobal(L, libname);
}

////// Somewhat experimental: push block
// This will crash and burn if the upvalue has been altered.
static int _cohlua_call_block(lua_State *L) {
    int (^block)() = cohlua_to_object(L, lua_upvalueindex(1));
    return block(L);
}

void cohlua_push_block(lua_State* L, int (^block)(lua_State *L)) {
    cohlua_push_object(L, block);
    lua_pushcclosure(L, _cohlua_call_block, 1);
}


////// Debugging stuff
// Output call stack as a char*.
const char* cohlua_print_runtime_stack(lua_State *L) {
    char *buff = malloc(sizeof(char[1024]));
    lua_Debug dbgInfo;
    int level = 0;
    int remainingBuffer = 1023;
    int printLen;
    
    while(lua_getstack(L, level, &dbgInfo)) {
        lua_getinfo(L, "nl", &dbgInfo);
        printLen = snprintf(buff, remainingBuffer, "(%d)  %s  (%d)\n", level, dbgInfo.name, dbgInfo.currentline);
        remainingBuffer -= printLen;
        
        level += 1;
    }
    
    return buff;
}

NSArray *cohlua_runtime_stack_as_NSArray(lua_State *L) {
    lua_Debug dbgInfo;
    int level = 0;
    NSMutableArray *stackInfo;
    
    while (lua_getstack(L, level++, &dbgInfo)) {
        lua_getinfo(L, "nl", &dbgInfo);
		[stackInfo addObject:@{
			 @"level": [NSNumber numberWithInt:level],
			 @"name": [NSString stringWithCString:dbgInfo.name encoding:NSASCIIStringEncoding],
			 @"line": [NSNumber numberWithInt:dbgInfo.currentline]
		}];
    }
    
    return stackInfo;
}
