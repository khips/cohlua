//
//  LuaLibrary.h
//  Cohlua
//
//  Created by Brian on 12/14/12
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

@class LuaState;

#import "lua.h"
#import "lauxlib.h"

@interface LuaLibrary : NSObject {
	__weak LuaState *luaState;
    char libName[128];
}

- (id)initWithName:(NSString *)name luaState:(LuaState *)lua;

- (void)addFunction:(lua_CFunction)cfunc withName:(NSString *)fname;
- (void)addBlock:(int (^)())block withName:(NSString *)fname;

- (void)loadFunctions:(luaL_Reg *)lib_f;

- (void)removeFunction:(NSString *)name;

@end
