//
//  CohluaObjectInterface.h
//  Objects conforming to this protocol have control over how code in
//  the Lua world can interact with them.
//
//  Created by Brian Sanders on 11/20/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *DefaultImplementation = @"ABC!@#$";

@protocol CohluaObjectInterface <NSObject>

@optional
// Returning DefaultImplementation from any of these methods will
// cause the default (Lua) behavior to take over.

// __index metamethod
- (id)cohluaIndexMethod:(NSString *)indexVal;
// __newindex metamethod
- (id)cohluaNewIndexKey:(NSString *)key value:(id)value;
// 

@end
