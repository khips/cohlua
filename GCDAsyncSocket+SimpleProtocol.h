//
//  GCDAsyncSocket+SimpleProtocol.h
//  ManiCard
//
//  Created by Brian Sanders on 11/10/12.
//
//

#import "GCDAsyncSocket.h"

@interface GCDAsyncSocket (SimpleProtocol)

- (void)writeLengthPrefixedData:(NSData *)data
                    withTimeout:(NSTimeInterval)timeout
                            tag:(long)tag;

@end
