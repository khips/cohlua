//
//  LuaErrorHandler.h
//  Cohlua
//
//  Created by Brian Sanders on 11/15/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LuaState;

@protocol LuaErrorHandler <NSObject>

- (void)lua:(LuaState *)luaState encounteredMemoryError:(NSString *)errString;
- (void)lua:(LuaState *)LuaState encounteredHandlerError:(NSString *)errString;

@end
