//
//  LuaState.m //  Cohlua
//
//  Created by brian on 11/11/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import "LuaState.h"

#import "lualib.h"
#import "cohlua_lua.h"

#import <objc/runtime.h>

//#import "GCDAsyncSocket+SimpleProtocol.h"


@interface LuaState (PrivateMethods)

- (void)pushVariableInNestedFields:(NSArray *)fields creatingTables:(BOOL)createTables;
- (void)pushVariableInNestedFields:(NSArray *)fields;
- (void)pushVariable:(NSString *)varName;

@end

static void cohlua_call_debug_hook(lua_State *L, lua_Debug *dbgInfo);

// Write data to the socket, prefixing it with a two-byte
// integer containing the length.
static NSUInteger write_prefixed(GCDAsyncSocket *socket, NSData *data, NSTimeInterval timeout, long tag) {
    NSUInteger dlen = [data length];
    char bytes[2];
    
    bytes[0] = dlen >> 8 & 255;
    bytes[1] = dlen & 255;
    
    [socket writeData:[NSData dataWithBytes:bytes length:2]
        withTimeout:0
                tag:0];
    [socket writeData:data withTimeout:timeout tag:tag];
    
    return dlen;
}

// Cohlua module.
static int cohlua_print_to_socket(lua_State *L) {
    GCDAsyncSocket *sock = cohlua_to_object(L, 1);
    NSString *message = cohlua_to_NSString(L, 2);

    write_prefixed(sock, [message dataUsingEncoding:NSASCIIStringEncoding],
            0, 1);

    return 0;
}

static int cohlua_json_output(lua_State *L) {
    GCDAsyncSocket *socket = cohlua_to_object(L, 1);
    NSData *json = cohlua_to_json_data(L, 2);
    lua_pop(L, 1);
    
    write_prefixed(socket, json, 0, 1);
    
    return 0;
}

static const struct luaL_Reg cohlua_lib_f[] = {
    {"output", cohlua_print_to_socket},
    {"json_output", cohlua_json_output},
    {"platform_info", cohlua_platform_info},
    {NULL, NULL}
};

static void cohlua_load_library(lua_State *L) {
    lua_newtable(L);
    luaL_setfuncs(L, cohlua_lib_f, 0);
    lua_setglobal(L, "cohlua");

    if(luaL_loadbuffer(L, (const char*)cohlua_lua, cohlua_lua_len, "cohlua")) {
        @throw [NSException exceptionWithName:@"LuaLibError"
                                       reason:@"Error loading 'cohlua' module."
                                     userInfo:nil];
    }
    if (lua_pcall(L, 0, LUA_MULTRET, 0)) {

        @throw [NSException exceptionWithName:@"LuaLibError"
                                       reason:@"Runtime error in 'cohlua' module"
                                     userInfo:nil];
    }
}


@implementation LuaState

@synthesize errorHandler=errorHandler_, state=L, debugMode=debugMode_;

#pragma mark - Lifetime Management
- (id)initWithLibrary:(BOOL)shouldLoad {
    self = [super init];
    
    if (self) {
        L = luaL_newstate();
        luaL_openlibs(L);
        
        if (shouldLoad)
            cohlua_load_library(L);
    }
    
    return self;
}

- (id)init {
    return [self initWithLibrary:YES];
}

- (void)dealloc {
    lua_close(L);
}

- (id)initWithRemoteInterfaceOnPort:(uint16_t)portnum {
    self = [self init];
    
    if (self) {
        [self startRemoteInterfaceOnPort:portnum];
    }
//    self.debugMode = YES;
    
    return self;
}

#pragma mark - Load library
- (void)loadLibrary {
    cohlua_load_library(L);
}

- (void)addLibraryFunction:(lua_CFunction)cfunc withName:(NSString *)fname {
    lua_getglobal(L, "cohlua");
    if (lua_isnil(L, -1)) {
        @throw [NSException exceptionWithName:@"LibraryNotLoaded"
                                       reason:@"Can't add function when library is not loaded"
                                     userInfo:nil];
    }
    lua_pushcfunction(L, cfunc);
    lua_setfield(L, -2, [fname cStringUsingEncoding:LUA_STRING_ENCODING]);
    lua_pop(L, 1);
}

- (void)addLibraryBlock:(int (^)())block withName:(NSString *)fname {
    lua_getglobal(L, "cohlua");
    cohlua_push_block(L, block);
    
}

#pragma mark - Properties
- (void)setDebugMode:(BOOL)debugMode {
    if (debugMode == debugMode_)
        return;
    
    debugMode_ = debugMode;
    
    if (debugMode) {
        lua_sethook(L, cohlua_call_debug_hook, LUA_MASKCALL, 0);
        cohlua_push_object(L, self);
        lua_setglobal(L, "_LuaState");
    } else {
        lua_sethook(L, cohlua_call_debug_hook, 0, 0);
        lua_pushnil(L);
        lua_setglobal(L, "_LuaState");
    }
}

#pragma mark - Running Code
- (void)loadFile:(NSString *)libPath {
    const char *libPathC = [libPath cStringUsingEncoding:LUA_STRING_ENCODING];
    if (luaL_dofile(L, libPathC)) {
        NSString *err = cohlua_to_NSString(L, -1);
        @throw [NSException exceptionWithName:@"LoadError"
                                       reason:err
                                     userInfo:nil];
    }
}

#pragma mark - Examining the stack
- (NSArray *)arrayWithStack {
    int top = lua_gettop(L);
    NSMutableArray *stack = [[NSMutableArray alloc] initWithCapacity:top];
    
    for (int i = 1; i <= top; i++) {
        [stack addObject:lua_to_objc(L, i)];
    }
    
    return stack;
}

- (id)objectAtIndex:(int)index {
    return lua_to_objc(L, index);
}

#pragma mark - Manipulating the Stack
// Push the value contained in the variable varName onto the
// top of the stack.
- (void)pushVariable:(NSString *)varName {
    NSArray *pieces = [varName componentsSeparatedByString:@"."];
    [self pushVariableInNestedFields:pieces creatingTables:NO];
}

// Push the value contained in the variable varName onto the
// stack.  Setters can call with creatingTables:YES to create
// all intermediate tables, as necessary and as possible.
- (void)pushVariableInNestedFields:(NSArray *)fields creatingTables:(BOOL)createTables; {
    NSString *key;
    const char* cKey;
    NSInteger lastField = [fields count] - 1;
    
    lua_pushglobaltable(L);
    for (int i = 0; i <= lastField; i++) {
        key = [fields objectAtIndex:i];
        cKey = [key cStringUsingEncoding:LUA_STRING_ENCODING];
        lua_getfield(L, -1, cKey);
        
        if (lua_isnil(L, -1) && createTables) {
            lua_pop(L, 1);
            lua_createtable(L, 0, 1);
            lua_pushvalue(L, -1); // duplicate table
            lua_setfield(L, -3, cKey);
        } else if (i != lastField && !lua_istable(L, -1)) {
            // pop the non-table value and its containing table:
            lua_pop(L, 2);
            @throw [VariableIsNotATable exceptionWithName:@"VariableIsNotATable"
                                                   reason:@"Invalid assignment to variable"
                                                 userInfo:@{@"variable" : key}];
        }
        
        lua_remove(L, -2);
    }
}

- (void)pushVariableInNestedFields:(NSArray *)fields {
    [self pushVariableInNestedFields:fields creatingTables:NO];
}

- (id)callFunction:(NSString *)funcName
      withErrHandler:(NSString *)handlerFn, ... __attribute__((sentinel(0, 1))) {
    
    if (handlerFn) {
        @try {
            [self pushVariable:handlerFn];
        }
        @catch (VariableIsNotATable *exception) {
            handlerFn = nil;
        }
    }
    
    @try {
        [self pushVariable:funcName];
    }
    @catch (VariableIsNotATable *exception) {
        lua_pushnil(L);
    }
    
    if (!lua_isfunction(L, -1)) {
        @throw [VariableIsNotAFunction exceptionWithName:@"VariableIsNotAFunction"
                                                  reason:@"Invalid function"
                                                userInfo:@{@"variable" : funcName}];
    }
    
    id retVal;
    
    va_list argp;
    id arg;
    int argCount = 0;
    
    va_start(argp, handlerFn);
    while (1) {
        arg = va_arg(argp, id);
        
        // final argument
        if (arg == nil)
            break;
        
        cohlua_push_from_objc_checked(L, arg);
        argCount++;
    }
    va_end(argp);
    
    int status = lua_pcall(L, argCount, 1, handlerFn ? (-argCount-2) : 0);
    switch (status) {
        case LUA_OK:
            retVal = lua_to_objc(L, -1);
            lua_pop(L, 1);
            return retVal;
        case LUA_ERRMEM:
            return nil;
    }
    
    return nil;
}

#pragma mark - Lua Variables
- (id)valueForVariable:(NSString *)varName {
    [self pushVariable:varName];
    id val = lua_to_objc(L, -1);
    lua_pop(L, 1);
    
    return val;
}

- (id)stringValueForVariable:(NSString *)varName {
    [self pushVariable:varName];
    cohlua_call_tostring(L, -1);
    NSString *sVal = cohlua_to_NSString(L, -1);
    lua_pop(L, 1);
    
    return sVal;
}

- (void)setVariable:(NSString *)varName toValue:(id)val creatingTables:(BOOL)createTables {
    NSArray *pieces = [varName componentsSeparatedByString:@"."];
    NSUInteger pieceCount = [pieces count];
    NSRange subRange;
    
    if (pieceCount == 0) return;
    
    const char *keyName = [pieces[pieceCount - 1] cStringUsingEncoding:LUA_STRING_ENCODING];
    
    subRange.location = 0;
    subRange.length = pieceCount - 1;
    [self pushVariableInNestedFields:[pieces subarrayWithRange:subRange] creatingTables:createTables];
    
    if (lua_istable(L, -1)) {
        cohlua_push_from_objc(L, val);
        lua_setfield(L, -2, keyName);
    } else {
        // error
    }
}

- (void)setVariable:(NSString *)varName toValue:(id)val {
    [self setVariable:varName toValue:val creatingTables:NO];
}

///  EXPERIMENTAL:
static int call_cfunc_implementation(lua_State* L) {
    Class classref = cohlua_to_object(L, lua_upvalueindex(1));
    SEL sel_ = cohlua_to_selector(L, lua_upvalueindex(2));
//    NSInvocation *invok = [NSInvocation invocationWithMethodSignature:[NSMethodSignature signatureWithObjCTypes:<#(const char *)#>]]
    
    return 1;
}

// Need to clean up the references held by the C closures.
static int cleanup_module_after_gc(lua_State* L) {
    return 0;
}

static NSString *methodNameForSelector(SEL sel_) {
    NSString *mName = NSStringFromSelector(sel_);
    return [mName stringByReplacingOccurrencesOfString:@":"
                                            withString:@"_"];
}

- (void)loadClass:(Class)defineClass asModule:(NSString *)moduleName {
    if (class_isMetaClass(defineClass)) {
        @throw [NSException exceptionWithName:@"InvalidModuleDefClass"
                                       reason:@"Class most not be a metaclass."
                                     userInfo:nil];
    }
    
    Class metaClass = object_getClass(defineClass);
    unsigned int methodCount;
    Method *methodList = class_copyMethodList(metaClass, &methodCount);
    
    SEL mSel;
    NSString *mName;
    
    lua_createtable(L, 0, methodCount);
    for (int i = 0; i <= methodCount; i++) {
        mSel = method_getName(methodList[i]);
        mName = methodNameForSelector(mSel);
        cohlua_push_object(L, defineClass);
        cohlua_push_selector(L, mSel);
    }
    
    free(methodList);
}


/////////////////////////////////////////////////////////////////////////////
/////                       Delegate Management                         /////
/////////////////////////////////////////////////////////////////////////////
#pragma mark - Delegate Handling
- (void)memoryError:(NSString *)errMesg {
    if (errorHandler_) {
        [errorHandler_ lua:self encounteredHandlerError:errMesg];
    } else {
        NSLog(@"Encountered memory error: %@", errMesg);
    }
}

- (void)handlerError:(NSString *)errMesg {
    if (errorHandler_) {
        [errorHandler_ lua:self encounteredHandlerError:errMesg];
    } else {
        NSLog(@"Encountered handler error: %@", errMesg);
    }
}


/////////////////////////////////////////////////////////////////////////////
/////                   Remote Debugging Interface                      /////
/////////////////////////////////////////////////////////////////////////////


#pragma mark - Remote Interface
- (void)startRemoteInterfaceOnPort:(uint16_t)portNum
                         withQueue:(dispatch_queue_t)queue {
    if (socket && [socket isConnected]) {
        return;
    }

    socket = [[GCDAsyncSocket alloc] initWithDelegate:self
                                        delegateQueue:queue];
    NSError *error;
    if (![socket acceptOnPort:portNum error:&error]) {
        NSLog(@"Error while setting up port: %@", error);
    }
}

- (void)startRemoteInterfaceOnPort:(uint16_t)portNum {
    return [self startRemoteInterfaceOnPort:3240
                                  withQueue:dispatch_get_main_queue()];
}

- (void)stopRemoteInterface {
    [socket disconnect];
}

// GCDAsyncSocketDelegate
- (void)readLine:(GCDAsyncSocket *)sock {
    [sock readDataToData:[GCDAsyncSocket CRLFData] withTimeout:-1 tag:1];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    if (tag == 1) {
        lua_getglobal(L, "cohlua");
        lua_getfield(L, -1, "_received_data");
        
        if (lua_isnil(L, -1)) {
            lua_pop(L, 2);
            @throw [NSException exceptionWithName:@"NoDataHandler"
                                           reason:@"No implementation of cohlua._received_data"
                                         userInfo:nil];
        }
        
        cohlua_push_object(L, sock);
        cohlua_push_NSString(L, [[NSString alloc] initWithBytes:[data bytes]
                                                         length:[data length]
                                                       encoding:NSASCIIStringEncoding]);
        
        lua_call(L, 2, 1);
        lua_pushboolean(L, 1);
        if (lua_compare(L, -2, -1, LUA_OPEQ)) {
            // If _received_data returns boolean true, close the socket.
            [sock disconnect];
            sock = nil;
        } else {
            [self readLine:sock];
        }
    }
}

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket {
    NSLog(@"Accepted new connection.");
    write_prefixed(newSocket, [@"- ManiCard debugging interface. -\n"
                               dataUsingEncoding:NSASCIIStringEncoding],
                   -1, 2);
    [self readLine:newSocket];
}

- (void)socketDidCloseReadStream:(GCDAsyncSocket *)sock {
    NSLog(@"Socket stream closed.");
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock
                  withError:(NSError *)err {
    NSLog(@"Socket disconnected.");
}

#pragma mark - Debugging
// registry:
static void cohlua_call_debug_hook(lua_State *L, lua_Debug *dbgInfo) {
    NSLog(@"Stack: %@", cohlua_stack_to_NSArray(L));
    
    lua_getglobal(L, "_LuaState");
    LuaState *lua = cohlua_to_object(L, -1);
    lua_pop(L, 1);
    
    [lua functionWasCalled:dbgInfo];
}

- (void)functionWasCalled:(lua_Debug *)dbgInfo {
    
}

@end
