//
//  LuaDebugHandler.h
//  Cohlua
//
//  Created by Brian Sanders on 11/24/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "lua.h"

@protocol LuaDebugHandler <NSObject>

- (void)functionWasCalled:(lua_Debug *)dbgInfo;

@end
