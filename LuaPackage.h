//
//  LuaPackage.h
//  Cohlua
//
//  Methods available through the cohlua module in Lua.
//
//  Created by Brian Sanders on 11/15/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "lua.h"
#import "lauxlib.h"

