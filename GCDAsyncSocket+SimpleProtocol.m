//
//  GCDAsyncSocket+SimpleProtocol.m
//  ManiCard
//
//  Created by Brian Sanders on 11/10/12.
//
//

#import "GCDAsyncSocket+SimpleProtocol.h"

@implementation GCDAsyncSocket (SimpleProtocol)

- (void)writeLengthPrefixedData:(NSData *)data
                    withTimeout:(NSTimeInterval)timeout
                            tag:(long)tag {
    NSUInteger dlen = [data length];
    char bytes[2];
    
    bytes[0] = dlen >> 8 & 255;
    bytes[1] = dlen & 255;
    
    [self writeData:[NSData dataWithBytes:bytes length:2]
        withTimeout:0
                tag:0];
    [self writeData:data withTimeout:timeout tag:tag];
}

@end
