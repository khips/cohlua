//
//  CohluaLua.h
//  Cohlua
//
//  A collection of functions for directly interacting with the
//  Lua state structure (lua_State).  Designed to be independent of
//  the LuaState wrapper.
//
//  Created by Brian Sanders on 11/17/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "lua.h"
#import "lauxlib.h"

#define LUA_STRING_ENCODING NSUTF8StringEncoding

// Examining values on the stack: /////////////////////////
BOOL cohlua_is_numeric_table(lua_State *L, int index);
BOOL cohlua_is_callable(lua_State *L, int index);

// Print the call stack.
const char* cohlua_print_runtime_stack(lua_State *L);
NSArray *cohlua_runtime_stack_as_NSArray(lua_State *L);

// Pushing values from Objective-C onto the Lua stack. ////
void cohlua_push_from_objc(lua_State *L, id val);
void cohlua_push_from_objc_checked(lua_State *L, id val);
void cohlua_push_selector(lua_State *L, SEL sel_);
void cohlua_push_NSString(lua_State *L, NSString *str);
void cohlua_push_items_from_NSArray(lua_State *L, NSArray *array);

// Push an object onto the stack.  The resulting userdata object (in
// Lua) will have its own metatable, but only the __gc function is
// implemented, to provide cleanup.
void cohlua_push_object(lua_State *L, id obj);
// Object with support for method calls. (Untested!!)
void cohlua_push_full_object(lua_State *L, id object);

// Retrieving lua values as Objective-C objects: //////////
id lua_to_objc(lua_State *L, int index);
id cohlua_to_object(lua_State *L, int index);
SEL cohlua_to_selector(lua_State *L, int index);
NSString *cohlua_to_NSString(lua_State *L, int index);
NSArray *cohlua_to_NSArray(lua_State *L, int index);
NSArray *cohlua_stack_to_NSArray(lua_State *L);
int cohlua_full_pcall(lua_State *L, int nargs, int nresults, int errfunc);

NSString* cohlua_to_printable(lua_State *L, int index);

// Calls
NSString* cohlua_call_tostring(lua_State *L, int index);

NSData* cohlua_to_json_data(lua_State *L, int index);

// Cohlua standard library functions: /////////////////////
// Functions that can be exposed to the Lua side.		 //

// Pushes a table containing information about the current platform.
int cohlua_platform_info(lua_State *L);
// Loads functions specified by the array of lua_Regs into the named
// global variable.
void cohlua_load_functions(lua_State *L, luaL_Reg *lib_f, char *libname);



// Push a new function onto the Lua stack that will call the
// specified block.
void cohlua_push_block(lua_State* L, int (^block)(lua_State *L));

// Exceptions

// Thrown if the user tries to retrieve a key from a nil
// variable.
@interface VariableIsNotATable : NSException
@end

@interface VariableIsNotAFunction : NSException
@end

@interface InsufficientStackSpace : NSException
@end
