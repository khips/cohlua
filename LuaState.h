//
//  LuaState.h
//  Cohlua
//
//  Created by Brian on 11/11/12.
//  Copyright (c) 2012 Brian Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CohluaLua.h"
#import "GCDAsyncSocket.h"

@class LuaState;

@protocol LuaErrorHandler <NSObject>

- (void)lua:(LuaState *)luaState encounteredMemoryError:(NSString *)errString;
- (void)lua:(LuaState *)LuaState encounteredHandlerError:(NSString *)errString;

@end

@interface LuaState : NSObject <GCDAsyncSocketDelegate> {
    lua_State *L;
    GCDAsyncSocket *socket;
    __weak id<LuaErrorHandler> errorHandler_;
    BOOL debugMode_;
}

@property (weak, nonatomic) id<LuaErrorHandler> errorHandler;
@property (nonatomic, readonly) lua_State* state;
@property (nonatomic, assign) BOOL debugMode;

- (id)initWithRemoteInterfaceOnPort:(uint16_t)portnum;

- (void)startRemoteInterfaceOnPort:(uint16_t)portNum
                         withQueue:(dispatch_queue_t)queue;

// Load the file at the specified path.
// Throws an exception if the file fails to load.
- (void)loadFile:(NSString *)libPath;

// Examining the stack:
- (NSArray *)arrayWithStack;
// Retrieve the object at the specified index and convert it
// to an appropriate Cocoa type.
- (id)objectAtIndex:(int)index;

// Lua Variables:
/**
 
 */
- (id)valueForVariable:(NSString *)varName;

/**
     Fetch the value of the named variable, call
     tostring() on it, and return the resulting
     string.
 */
- (id)stringValueForVariable:(NSString *)varName;
/**
    Set the value of a variable.
 */
- (void)setVariable:(NSString *)varName toValue:(id)val;
/**
    Set the value of a variable, creating intermediate tables
    as necessary.  For example, when setting mytab.a = 1,
    the mytab table will be created if it does not already
    exist.
 */
- (void)setVariable:(NSString *)varName toValue:(id)val
     creatingTables:(BOOL)createTables;


// Experimental:
- (void)loadClass:(Class)defineClass asModule:(NSString *)moduleName;


// Delegate management
- (void)memoryError:(NSString *)errMesg;


// Remote interface:
- (void)startRemoteInterfaceOnPort:(uint16_t)portNum;
- (void)stopRemoteInterface;
//- (void)setHandler:() ForCommand:(NSString *)cmdPref


// Debugging:
- (void)functionWasCalled:(lua_Debug*)dbgInfo;
@end
